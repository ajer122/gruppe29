#if !defined(__stoler_H) 
#define __stoler_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"

using namespace std;

class Stoler : public Sone {
	private:
		int rader;
		int seter;
		char* def;
		int** billett;
	public: 
		Stoler(ifstream& inn, char* navn, bool oppsett = false); 
		Stoler(char* navn);
		Stoler(Stoler & s);
		~Stoler();
		bool operator == (char s);
		void nyttOppsett();
		void display();
		void skriv_til_fil(ofstream& ut, bool oppsett = false);
		void skriv_billett_til_fil(int rad, int antbill, int n);
		void display_billetter();
		void kjop_billett(int n);
	
};

#endif