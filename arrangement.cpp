#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include "funksjoner.h"
#include "kunder.h"
#include "arrangementer.h"
#include "arrangement.h"
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "stoler.h"
#include "vrimle.h"
#include "enum.h"
#include "meny_funksjoner.h"
#include "listtool2.h"

using namespace std;

Arrangement::Arrangement(ifstream & inn, char* arrNavn) : Text_element(arrNavn) {
	int type;
	char buffer[STRLEN];
	inn>> arrNr >> dato >> time >> min;						 //Leser inn inter.
	inn.ignore();
	inn.getline(buffer, STRLEN);
	spillested = new char[strlen(buffer)+1];			//Leser inn spillested.
	strcpy(spillested, buffer);
	inn.getline(buffer,STRLEN);
	artist = new char[strlen(buffer)+1];					//Leser inn artist.
	strcpy(artist, buffer);
		inn >> type;										//Leser inn enum.
	switch (type){
		case 0 : typeArrangement = Musikk; break;
		case 1 : typeArrangement = Sport; break;
		case 2 : typeArrangement = Teater; break;
		case 3 : typeArrangement = Show; break;
		case 4 : typeArrangement = Kino; break;
		case 5 : typeArrangement = Familie; break;
		case 6 : typeArrangement = Festival; break;
	}
	inn.ignore();
}

Arrangement::Arrangement(char* arrNavn, char* navn, int nummer) : Text_element(arrNavn) {
	char buffer[STRLEN];
	int nr;	
	extern Steder stedbase;
	
	arrNr = nummer;				 //Legger nummer fra Arrangementer inn i arrNr.
		
	spillested = new char [strlen(buffer)+1]; 
	strcpy(spillested, navn);	//Legger inn navn fra Arrangementer inn i spillested.	

	les("\nNavn på artist", buffer, STRLEN);		//Leser inn navn på artist.
	artist = new char [strlen(buffer)+1];
	strcpy(artist, buffer);

	dato = les("Dato (AAADDMM)", MINDATO, MAXDATO);			  //Leser inn dato.

	cout << "\nTidspunkt:";
	time = les("Time", 0, 23);								 //Leser inn timer.
	min = les("Minutt", 0, 59);									// og minutter.

	enumtype();				   //Henter inn arrangement type fra enum funksjon.

	nr = les("Hvilket oppsett skal legges inn?",MINOPPSETT,MAXOPPSETT);
	oppsett = stedbase.hent_oppsett(spillested, nr);//Kopierer oppsett fra Steder.

	skriv_til_arr_fil();			   //Sender oppsett og skriver til fil.
}

void Arrangement::display() {		//Displayer basic informasjon om arrangementet.
	cout << "\n\nArrangementsnavn: " << text;
	cout << "\nArtist: " << artist;
	cout << "\nSpillested: " << spillested;
	cout << "\nDato: " << dato << "\nTidspunkt: " << time << ':'<< min;
	cout << "\nType arrangement: "; vis_enum();
}

void Arrangement::display_alt(){		//Displayer all data om arrangementet.
	Sone *temp, *kopi;					//Midlertidig sone-objekt
	int antOppsett;

	cout << "\n\nArrangementsnavn: " << text;
	cout << "\nArtist: " << artist;
	cout << "\nSpillested: " << spillested;
	cout << "\nDato: " << dato << "\nTidspunkt: " << time << ':'<< min;
	cout << "\nType arrangement: "; vis_enum(); cout << endl;
	les_fra_arr_fil();						//Leser inn informasjon fra fil.
	
	antOppsett = oppsett->no_of_elements();	//Teller opp antall objekter i listen

	for (int i = 1; i <= antOppsett; i++) {
		temp = (Sone*)oppsett->remove_no(1);//Henter ut objekt fra listen
		if (*temp == 'S') {					//Sjekker objekt-type gjennom overloading 
			Stoler* kopi = (Stoler*) temp;	//Caster om til riktig type
			kopi->display_billetter();	    //Sender til aktuell display-funksjon
		}
		else {
			Vrimle* kopi = (Vrimle*) temp;	///Caster om til riktig type
			kopi->display_billetter();	    //Sender til aktuell display-funksjon
		}
	}
		delete oppsett;					    //Sletter oppsett fra hukommelsen
}

bool Arrangement::nr_sjekk(int n){	//Sjekker om Arrangement nr er like.
	if(n == arrNr){					//med variabel sendt.
	return true;
	}
	else return false;
}

bool Arrangement::type_sjekk(int n){		//Sjekker om type arrangement er like.
	if(n == typeArrangement){			//med variabel sendt.
		return true;
	}
	else return false;

}

bool Arrangement::dato_sjekk(int n){		//Sjekker om dato er like
	if(n == dato){						//med variabel sendt.
	return true;
	}
	else return false;
}

bool Arrangement::deler_navn(char* n) {
	if(strstr(text, n)){							//Sjekker om deler av text.
		return true;
	}
	else if (strstr(artist, n)){				  //Sjekker om deler av artist.
		 return true;
	}
	else if (strstr(spillested, n)){		  //Sjekker om deler av spillested.
		return true;
	}
	else
		return false;
}

void Arrangement::skriv_til_arr_fil() {//Henter inn oppsett fra Arrangement constructor.
	int antSoner;
	Sone *temp, *kopi;
	char filnavn[NVNLEN];
	char filbuffer[MAXARRNUMMER];

	antSoner = oppsett->no_of_elements();//Legger soner fra oppsett inn i ny variabel.

	strcpy(filnavn, "ARR_");					   //Skriver fil med navn ARR_
	strcat(filnavn, _itoa(arrNr, filbuffer, 10));  //Henter arrNr til fil ARR_arrNr.DTA.
	strcat(filnavn, ".DTA");					   //skriver fil med navn .DTA.


	ofstream ut(filnavn);
		ut << antSoner << '\n';					   //Skriver inn anntall soner.
		for (int i = 1; i <= antSoner; i++) {	   //Teller gjennom soner.
			temp = (Sone*)oppsett->remove_no(1);   //Tar ut sonen og legger i temp peker.
			if (*temp == 'S') {					   //Sjekker om Sone med Stoler.
				Stoler* kopi = (Stoler*) temp;	   //legger sone inn i kopi peker.
				kopi->skriv_til_fil(ut, true);	   //Skriver Stol sone til fil.
			}
			else {
				Vrimle* kopi = (Vrimle*) temp;	//Legger sone inn i kopi peker.
				kopi->skriv_til_fil(ut, true);	 //Skriver Vrimle sone til fil.
			}				
		}
		delete oppsett;
}

void Arrangement::billett() {
	int n;
	char sone[NVNLEN];
	extern Kunder kundebase;
	Sone *temp, *kopi;					//Midlertidig sone-objekt

	n = les("\nKundenummer", KUNDENR, 99999); //Leser kundenummer
	if(kundebase.sjekk_kundenummer(n)) {	  //Sjekker om kundenummer eksisterer
		les("\nI hvilken sone onsker kunden billett?", sone, NVNLEN); //Leser sonenavn
		les_fra_arr_fil();					         	//Leser inn data fra ARR_XX.DTA
		if(oppsett->in_list(sone)) {					//Sjekker om sone eksisterer
			temp = (Sone*)oppsett->remove(sone);		//Henter ut sone-objekt
			if (*temp == 'S') {							//Sjekker objekt-type gjennom overloading
				Stoler* kopi = (Stoler*) temp;			//Caster om til riktig objekt
				kopi->kjop_billett(n);					//Sender videre til aktuell billett-funksjon
				oppsett->add(kopi);						//med medsendt kundennummer og legger tilbake i listen
			}
			else {
				Vrimle* kopi = (Vrimle*) temp;			//Caster om til riktig objekt
				kopi->kjop_billett(n);					//Sender videre til aktuell billett-funksjon
				oppsett->add(kopi);						//med medsendt kundennummer og legger tilbake i listen
			}
			skriv_til_arr_fil();						//Skriver ut nye data til ARR_XX.DTA
		}
		else 
			cout << "\nFinner ikke angitt sone";
	}
	else
		cout << "\nFinner ikke kunde";
}

void Arrangement::skriv_til_fil(ofstream& ut) {	
	ut << text <<'\n';
	ut << arrNr << ' ' << dato << ' ' << time << ' ' << min << '\n';
	ut << spillested << '\n';
	ut << artist << '\n';
	ut << typeArrangement << '\n';
} 

void Arrangement::les_fra_arr_fil() {	
	char buffer[NVNLEN], type;
	char* temp;
	int antSoner;
	char filnavn[NVNLEN];
	char filbuffer[MAXARRNUMMER];

	strcpy(filnavn, "ARR_");				   	   //Lager filnavn ved å sette sammen
	strcat(filnavn, _itoa(arrNr, filbuffer, 10));  //ARR_ med arrangementsnummeret
	strcat(filnavn, ".DTA");					   //og .DTA


	ifstream inn(filnavn);					//Finner filen
	
	if(inn) {							    //Sjekker om filen eksiterer
		oppsett = new List(Sorted);			//Oppretter en sortert liste.

		inn >> antSoner;							 //Leser inn antall soner.

		for(int i = 1; i <= antSoner; i++) {		 //Går gjennom alle sonene
			inn.ignore();			
			inn.getline(buffer, NVNLEN);			//Leser inn navn på sonen
			temp = new char[strlen(buffer)+1];		   
			strcpy(temp, buffer);	
			inn >> type;							//Leser inn sone-type
				if(type=='S')						     //Hvis type er S:
					oppsett->add(new Stoler(inn, temp, true));//Legger inn nytt stoler-objekt i listen
				if(type=='V')							//Hvis type er V:
					oppsett->add(new Vrimle(inn, temp, true));//Legger inn nytt vrimle-objekt i listen
		} 
		cout << "\nDATA LEST FRA " << filnavn << "!\n";
	}
	else
		cout << "\nFinner ikke " << filnavn;
	inn.close();
}

void Arrangement::enumtype(){
	int n;

	n = les("Type arrangement( Musikk, Sport, Teater, Show, Kino, Familie, Festival)", 0, 6);

	if(n == 0) typeArrangement = Musikk;//Legger riktig type enum inn i variabel.
	if(n == 1) typeArrangement = Sport;	//Legger riktig type enum inn i variabel.
	if(n == 2) typeArrangement = Teater;//Legger riktig type enum inn i variabel.
	if(n == 3) typeArrangement = Show;//Legger riktig type enum inn i variabel.
	if(n == 4) typeArrangement = Kino;//Legger riktig type enum inn i variabel.
	if(n == 5) typeArrangement = Familie;//Legger riktig type enum inn i variabel.
	if(n == 6) typeArrangement = Festival;//Legger riktig type enum inn i variabel.
}

void Arrangement::vis_enum(){
		char *arrtype[] = //Lager array's med string.
		{ "Musikk", "Sport", "Teater", 
		"Show", "Kino", "Familie", "Festival"};

		cout << arrtype[typeArrangement]; //Gjør om enum til string.
}
