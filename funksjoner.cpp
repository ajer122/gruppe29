#include <iostream>									
#include <fstream>	
#include <cstring>
#include <string>								
#include "meny_funksjoner.h"								
#include "arrangementer.h"														
#include "kunder.h"															
#include "steder.h"						

using namespace std;

char les(char t[])  {        //  Henter ett ikke-blankt upcaset tegn:
	char ch;
	cout << '\n ' << t << ":  ";   //  Skriver medsendt ledetekst.
	cin >> ch;	cin.ignore();    //  Leser ETT tegn. Forkaster '\n'.
	return (toupper(ch));         //  Upcaser og returnerer.
}

//  Leser et tall i et visst intervall:
int les(const char t[], const int min, const int max)  {
	int n;
	char buffer[STRLEN];
	do  {                                  // Skriver ledetekst:
		cout << "\n " << t << " (" << min << '-' << max << "): ";
		cin.getline(buffer, STRLEN);          // Leser inn ett tall.
	} while ((!(n=atoi(buffer)) || buffer[0]!=0) && (n < min || n > max));         // Sjekker at i lovlig intervall.
	return n;                             // Returnerer innlest tall.
}
                             //  Leser inn en ikke-blank tekst:
void les(const char t[], char s[], const int LEN) {  
 	do  {
   		cout << "\n " << t << ": ";    //  Skriver ledetekst.
    	cin.getline(s, LEN);          //  Leser inn tekst.
  	} while (strlen(s) == 0);       //  Sjekker at tekstlengden er ulik 0.
}

