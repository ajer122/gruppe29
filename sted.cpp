#include <iostream>
#include <cstring>
#include <string.h>
#include <fstream>
#include "string.h"
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "vrimle.h"
#include "stoler.h"
#include "const.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"
#include "listtool2.h"

using namespace std;

//Constructor for les fra fil
Sted::Sted(char* t, ifstream& inn) : Text_element(t) {
	char buffer[NVNLEN], type;		//Buffer for å lese inn navn
	char* temp;						//Temp char for å lese inn navn
	int antSoner;					//Int med antall soner

	for(int i = 1; i <= MAXOPPSETT; i++) //Initierer ny liste for oppsett
	oppsett[i] = new List(Sorted);	

	inn >> antOppsett;				//Leser inn antall oppsett

	for(int i = 1; i <= antOppsett; i++) {
		inn >> antSoner;			//Leser inn antall soner i oppsettet
			for(int j = 1; j <= antSoner; j++) {	
			inn.ignore();
			inn.getline(buffer, NVNLEN);
			temp = new char[strlen(buffer)+1];
			strcpy(temp, buffer);	
			inn >> type;
				if(type=='S')		//Sjekker om det er stolobjekt gjennom overloading av constructor
					oppsett[i]->add(new Stoler(inn, temp)); //Oppretter nytt stolobjekt og sender med navn
				if(type=='V')		//Sjekker om det er vrimleobjekt gjennom overloading av constructor
					oppsett[i]->add(new Vrimle(inn, temp)); //Oppretter nytt virmleobjekt og sender med navn
		}	
	}
} 

//Constructor for nytt sted
Sted::Sted(char* t) : Text_element(t)	{
	antOppsett = 0;						 //Nullstiller antall oppsett
	for(int i = 1; i <= MAXOPPSETT; i++) //Initierer ny liste for oppsett
		oppsett[i] = new List(Sorted);	 
}

void Sted::nytt_oppsett() {				//Oppretter nytt oppsett
	int n = antOppsett+1; 

	if(oppsett[MAXOPPSETT]->is_empty()) {	//Sjekker at det er plass 
		antOppsett++;						//Plusser på antall oppsett
		cout << "\nNytt oppsettnummer " << n << " er opprettet"; 
		default_oppsett(n);  				//Sender til default_oppsett() for å legge inn og endre soner
	}
	else
		cout << "\nDet er ikke plass til flere oppsett!\n";
} 

void Sted::endre_oppsett() {		
	int n = 1;							//Settes til 1 for å sjekke om første
	if(!oppsett[n]->is_empty()) {;		//skuff er tom eller ikke
		n = les("\nEndre pa oppsettnummer", 1, antOppsett);
   			if(!oppsett[n]->is_empty())
   				default_oppsett(n);		//Sender til default_oppsett() for å legge inn og endre soner
	}
	else
    	cout << "\nIngen oppsett a endre!\n"; 
 }

void Sted::default_oppsett(int n) {
	char valg;

		meny_oppsett();
		valg = les_kommando();		
		while (valg!='Q') {
			switch(valg) {
				case 'N': ny_sone(n); 		  	break;
				case 'E': endre_sone(n); 	  	break;
				case 'K': kopier_oppsett(n); 	break;
				case 'S': 					   	break;
				case 'D': display_oppsett();	break;
				default:			          	break;
			}
			meny_oppsett();
			valg = les_kommando();	
		}
}

void Sted::ny_sone(int n){ 
		char navn[NVNLEN], valg;
		les("\nNavn pa ny sone", navn, NVNLEN);

		if (!oppsett[n]->in_list(navn)) { //Sjekker sonenavnet eksisterer
			meny_sone();				  
			valg = les_kommando();
			switch(valg) {
				case 'S': oppsett[n]->add(new Stoler(navn)); break; //Oppretter nytt stoler-objekt
				case 'V': oppsett[n]->add(new Vrimle(navn)); break; //Oppretter nytt vrimle-objekt
				default: cout << "\nUgyldig kommando!\n"; break;
			}
		}
		else 
			cout << "\nSonenavnet er allerede i bruk!\n";
}

void Sted::endre_sone(int n) {
	char navn[NVNLEN], buffer[NVNLEN], valg;
	
	les("\nNavn pa sonen du vil endre", navn, NVNLEN);

	if (oppsett[n]->in_list(navn)) {		 //Sjekker sonenavnet eksisterer
		les("\nNytt navn på sonen", buffer, NVNLEN);
		if (!oppsett[n]->in_list(buffer)) { 
			meny_sone(); 
			valg = les_kommando();		
			switch(valg) {					//Sletter eksisterende objekt og oppretter nytt
				case 'S': oppsett[n]->destroy(navn); oppsett[n]->add(new Stoler(buffer)); break;
				case 'V': oppsett[n]->destroy(navn); oppsett[n]->add(new Vrimle(buffer)); break;
				default: cout << "\nUgyldig kommando!\n"; break;
			}
		}
		else 
			cout << "\nSonenavnet er allerede i bruk!\n";
	}
	else 
		 cout << "\n\nIngen sone med dette navnet!\n"; 
}

void Sted::kopier_oppsett(int n) {
	int nr;
	nr = les("Hvilket oppsett ønsker du kopiere?", 1, antOppsett);
	if(!(nr==n)) {							//Sjekker at det ikke skrives inn samme 
		if(!oppsett[nr]->is_empty()) {		//oppsettnummer som man ønsker kopiere til
			oppsett[n] = kopier(nr);		//Sender videre til copy-constructoren
			cout << "Oppsett " << nr << " kopiert";
		}
	else
		cout << "Du kan ikke kopiere samme oppsett!\n";
	}
}

List* Sted::kopier(int n) {		//Copy-constructor
	List* liste = NULL;			//Nullstiller midlertidig liste
	int i, ant;
	Sone *sone, *kopi;			//Midlertidig sone-objekter
	if (n >= 1 && n <= antOppsett) {		//Sjekker at oppsettet finnes
  		ant = oppsett[n]->no_of_elements();	//Antall objekter å kopiere
  		liste = new List(Sorted);			//Definerer midlertidig sortert liste
  		for (i = 1; i <= ant; i++) {		//Går gjennom antall objekter
  			sone = (Sone*) oppsett[n]->remove_no(i);	//Henter ut sone-objekt
  			if (*sone == 'S') kopi = new Stoler(*((Stoler*) sone)); //Sjekker type gjennom overloading av constructor
  						else  kopi = new Vrimle(*((Vrimle*) sone)); //og caster om til riktig objekt
  			oppsett[n]->add(sone);		//Legger tilbake i oppsett
  			liste->add(kopi);			//Legger inn i midlertidige listen
  		}
  	}
  	return liste;  		//Retunerer den midlertidige, kopierte listen
}

void Sted::display() {	
	cout << text;
	cout << "\nAntall oppsett: " << antOppsett << endl;
}

void Sted::display_oppsett() {
	int nummer;						//Int for oppsettnummer
	nummer = les("\nOppsettnummer", MINOPPSETT, MAXOPPSETT);
   	if(!oppsett[nummer]->is_empty())	 //Hvis oppsettet ikke er tomt
    	oppsett[nummer]->display_list(); //skriver den alle data om oppsettet til skjermen
	else
    	cout << "\nOppsett " << nummer << " er tomt\n"; 
}

void Sted::skriv_til_fil(ofstream& ut) {	
	int antSoner;					//Int for antall soner i hvert oppsett
	Sone *temp, *kopi;				//Midlertidige sone-objekter
	char filnavn[NVNLEN];		
	char filbuffer[MAXARRNUMMER];

	ut << text << '\n';				//Skriver ut navnet på stedet
	ut << antOppsett << '\n';		//og antall oppsett som skal skrives ut
	for ( int i = 1; i <= antOppsett; i++) {	  //Går gjennom oppsettene og skriver ut
		antSoner = oppsett[i]->no_of_elements();  //ut antall soner i hvert oppsett
		ut << antSoner << '\n';					

		for (int j = 1; j <= antSoner; j++) {		//Går gjennom alle sonene
			temp = (Sone*)oppsett[i]->remove_no(j);	//og henter de ut fra liste
			if (*temp == 'S') {						//Sjekker hva slags objekt det er gjennom 
				Stoler* kopi = (Stoler*) temp;		//overloading  og caster om til riktig
				kopi->skriv_til_fil(ut);			//Sender videre til skriv-funksjonen i objektet
				oppsett[i]->add(temp);				//Legger tilbake i listen
			}
			else {
				Vrimle* kopi = (Vrimle*) temp;
				kopi->skriv_til_fil(ut);
				oppsett[i]->add(temp);
			}
		}
	}
}