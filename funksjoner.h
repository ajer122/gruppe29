#if !defined(__funksjoner_H) 
#define __funksjoner_H 

#include <iostream>								
#include <cstring>
#include <string>

using namespace std;

char les(char t[]);
int les(const char t[], const int min, const int max);
void les(const char t[], char s[], const int LEN);

#endif