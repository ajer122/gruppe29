#if !defined(__kunder_H) 
#define __kunder_H 

#include <iostream>
#include "kunde.h"
#include "listtool2.h"

using namespace std;

class Kunder {
	private:
		int sisteKundeNr; 
		List* KundeListe;
	public:
		Kunder();
		~Kunder();		
		void kunde_display();
		void display();
		void ny_kunde();
		void endre_kunde();
		bool sjekk_kundenummer(int n);
		void skriv_til_fil();	
		void skriv_billett_til_fil(ofstream &ut,int n);
		void les_fra_fil();
		void handling();
};

#endif