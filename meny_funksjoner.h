#if !defined(meny_funksjoner_h)
#define meny_funksjoner_h

#include <iostream>
#include <cstring>
#include <string>

using namespace std;

char les_kommando();
void skriv_meny();
void meny_oppsett();
void meny_sone();
void meny_kunde_display();
void meny_arrangement_display();

#endif