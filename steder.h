#if !defined(__steder_H) 
#define __steder_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"
#include <fstream>
#include "listtool2.h"

using namespace std;

class Steder {
		List* StedListe;
	public: 
		Steder();
		~Steder();
		void nytt_oppsett();
		void endre_oppsett();
		void display_oppsett();
		void nytt_sted();
		void display();
		List* Steder::hent_oppsett(char* navn, int nr);
		void skriv_til_fil();
		void les_fra_fil();
		void handling(char a);	
		bool sjekk_navn(char* navn);
};

#endif