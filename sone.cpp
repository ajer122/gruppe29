#include <iostream>
#include <cstring>
#include <string>
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "vrimle.h"
#include "stoler.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"

using namespace std;
//Constructor for lesing fra fil
Sone::Sone(ifstream& inn, char* navn) : Text_element(navn) { 
	inn >> tilSalgs >> pris >> solgt;
}
//Constructor
Sone::Sone(char* navn) : Text_element(navn) {
}
//Counstructor for kopiering av oppsett
Sone::Sone(Sone* s) : Text_element(s->text) {
		tilSalgs = s->tilSalgs;
		pris = s->pris;		
		solgt = s->solgt;
}
//Overloaded operator som sjekker sone-type
bool Sone::operator == (char s) { 
	return 0; 
}

void Sone::display() { 
	cout << "\n\n********  " << text << "  **********" <<endl;
}

void Sone::skriv_til_fil(ofstream& ut) {
}

void Sone::les_fra_fil() {
}

void Sone::billett_kjop() {

}