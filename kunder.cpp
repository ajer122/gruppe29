#include <iostream>
#include <fstream>
#include "kunder.h"
#include "kunde.h"
#include "const.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"
#include "listtool2.h"

using namespace std;

//Constructor
Kunder::Kunder() {
	KundeListe = new List(Sorted); //Initierer sortert liste med Kunder
	sisteKundeNr = 10000;		   //Definerer kundenummer
}

Kunder :: ~Kunder() {
	//Foreløpig tom
}

void Kunder::les_fra_fil() {
	int n;				//Int for kundenummer
	Kunde* temp; 		//Midlertidig kunde-objekt

	ifstream inn("KUNDER.DTA"); //Finner filen
	if(inn) {					//Sjekker om filen eksisterer
		inn >> sisteKundeNr; 	//Leser inn siste kundenummer
								//Går gjennom alle kundenummer på fil
		for(int i = KUNDENR; i <= sisteKundeNr; i++) {
 		    inn >> n;			//Leser inn kundenummer
 			inn.ignore();
			temp = new Kunde(inn, n); //Oppretter ny kunde på medsendt kundenummer
 			KundeListe->add(temp); //Legger innlest data inn i et nytt objekt i kundelisten
 		}
	cout << "\nLEST DATA FRA KUNDER.DTA";
	}
	else
		cout << "\nFINNER IKKE FILEN\n";	
	inn.close();
}

void Kunder::handling() { //Undermeny som leser bokstav etter K
	char valg;
	
	cin >> valg;
	cin.ignore();

	switch (valg)  {
		case 'D': kunde_display();	break;
		case 'N': ny_kunde();		break;
		case 'E': endre_kunde();	break;
		default: break;
	}
}

void Kunder::kunde_display() {
	Kunde* temp; 		//Midlertidig kunde-objekt
	int n; 				//Int for kundenummer
	char valg;
	char navn[STRLEN];
	int antKunde;		//Int for antall kunder å gå gjennom

	meny_kunde_display();
	valg = les_kommando();

	if(valg == 'A') {                  //Hvis bruker velger A
		if (!KundeListe->is_empty()) {   //og listen ikke er tom
			KundeListe->display_list();//kalles alle kunders display-funksjon
			cout << "\n\n Trykk Enter for a ga tilbake til menyen.....";
			cin.ignore();				//Stopper menyen fra å skrives umiddelbart for å lettere
		}								//lese store mengder data som vises på skjermen
		else
			cout <<"\nIngen kunder a vise!\n";
	}
	if(valg == 'K') {  								//Hvis bruker velger K
		n = les("\nKundenummer: ", KUNDENR, 99999); //leses kundenummer inn
		if(KundeListe->in_list(n)) {				//Hvis kunden finnes
			temp = (Kunde*) KundeListe->remove(n);  //hentes objektet ut
			temp->display();						//og kaller display-funksjonen 
			KundeListe->add(temp);					//Legger objektet tilbake i listen
		}
		else
			cout <<"\nFinner ikke kundenr!\n";
	}
	if (valg == 'N'){								//Hvis bruker velger N
		antKunde = KundeListe->no_of_elements();	//Legger anntall noder i int variabel
		les("Skriv inn kundenavn", navn, STRLEN);	//Ber om et kundenavn
		for(int i = 1; i<=antKunde; i++){			//Går gjennom alle kundene i listen
			temp = (Kunde*) KundeListe->remove_no(i);//Tar ut en og en fra listen.
			if(temp->delerNavn(navn)){				//Sjekker deler av navn
				temp->display();					//Displayer alle navn med deler
			}
			KundeListe->add(temp);					//Legger tilbake i listen
		}
	}
}

void Kunder::ny_kunde() {
	Kunde* temp;							//Midlertidig kunde-objekt
	temp = new Kunde(++sisteKundeNr);		//Oppretter ny kunde med kundenummer
	KundeListe->add(temp);					//Legger objektet inn i listen
	cout << "\n\nNy kunde opprettet!";
	skriv_til_fil();
}

void Kunder::endre_kunde() {
	Kunde* temp; 				//Midlertidig kunde-objekt
	int n;						//Int for kundenummer
	n = les("\nKundenummer: ", KUNDENR, 99999); //Leser kundenummer
	if (KundeListe->in_list(n)) {				//Hvis kunden eksisterer
		temp = (Kunde*) KundeListe->remove(n);	//hentes objektet ut
		temp = new Kunde(n);					//og ny kunde opprettes på samme nummer
		KundeListe->add(temp);					//Legger tilbake i listen
	skriv_til_fil();							//Skriver nye data til fil
	}
	else
		cout <<"\nFinner ikke kundenr";
}

bool Kunder::sjekk_kundenummer(int n) { //Sjekker om kundenummer eksisterer	
	if (KundeListe->in_list(n)) 				
		return true;
	else return false;
}

void Kunder::skriv_til_fil() {
	Kunde* temp;					//Midlertidig kunde-objekt

	ofstream ut("KUNDER.DTA");		//Lager filnavn
		ut << sisteKundeNr << '\n'; //Skriver ut siste kundenummer først i filen
	
		if (!KundeListe->is_empty()){	//Hvis det er objekter i kundelisten
			for (int i = KUNDENR; i <= sisteKundeNr; i++){ //går den gjennom fra 
				temp = (Kunde*)KundeListe->remove(i);	   //definert kundenummer 
				temp->skriv_til_fil(ut);				   //til siste brukte, henter ut objektet
				KundeListe->add(temp);				       //og kaller på skriv-funksjonen
			}											   //før den legges tilbake
		} 
		else
			cout << "Fil ikke funnet";
}

void Kunder::skriv_billett_til_fil(ofstream &ut,int n){
	Kunde* temp;					//Midlertidig kunde-objekt

	if (!KundeListe->is_empty()) {	//Hvis det er objekter i kundelisten
		temp = (Kunde*)KundeListe->remove(n);	   //definert kundenummer 
		temp->skriv_billett_til_fil(ut, n);				   //til siste brukte, henter ut objektet
		KundeListe->add(temp);				       //og kaller på skriv-funksjonen
	}											   //før den legges tilbake
}

