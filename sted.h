#if !defined(__sted_H) 
#define __sted_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"

using namespace std;

class Sted : public Text_element {
	private:
		int antOppsett;
		List* oppsett[MAXOPPSETT+1];
	public:
		Sted(char* t, ifstream& inn);
		Sted(char* t);
		void nytt_oppsett();
		void endre_oppsett();
		void default_oppsett(int n);
		void ny_sone(int n);
		void endre_sone(int n);
		void kopier_oppsett(int n);
		List* kopier(int nr);
		void display();
		void display_oppsett();
		void skriv_til_fil(ofstream& ut);
};

#endif