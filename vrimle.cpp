#include <iostream>
#include <cstring>
#include <string>
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "kunder.h"
#include "vrimle.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"

using namespace std;
//Constructor for lesing fra fil
Vrimle::Vrimle(ifstream& inn, char* navn, bool oppsett) : Sone(inn, navn) {
	if(oppsett) {
		billett = new int[tilSalgs+1]; 
		
		for ( int i = 1; i <= tilSalgs; i++)
			inn >> billett[i];
	}
}
//Constructor
Vrimle::Vrimle(char* navn) : Sone(navn) {
	tilSalgs = les("\nAntall plasser", 1, MAXVRIMLE);
	solgt = 0;
	pris = les("\nBillettpris: ", 1, MAXPRIS);

	cout << "\n\nSone '" << navn << "' med " << tilSalgs << " vrimleplasser  til " << pris << ",- per billett opprettet";
}

Vrimle::Vrimle(Vrimle& v) : Sone((Sone*) &v) {
	billett = new int[tilSalgs+1]; 

	for ( int i = 1; i <= tilSalgs; i++)
			billett[i] = 0;
}
//Overloaded operator som sjekker sone-type
bool Vrimle::operator == (char s) {
	if (s == 'V') return true;
	else return false;
}

void Vrimle::display() {	//Display-funksjon som viser grov oversikt 
	Sone::display();
	cout << endl << tilSalgs << " vrimleplasser" << endl;
	cout << "\nAntall plasser: " << tilSalgs;
	cout << "\nPris per billett: " << pris;
}

void Vrimle::display_billetter() {	//Display-funksjon som viser detaljert oversikt 
	Sone::display();				//med informasjon om salg av billetter i sonen
	int ledig = tilSalgs - solgt;
	cout << endl << tilSalgs << " vrimleplasser" << endl;
	cout << "\nAntall billetter til salgs: " << ledig;
	cout << "\nPris per billett: " << pris;
	cout << "\nAntall billetter solgt: " << solgt << endl;
}

void Vrimle::skriv_til_fil(ofstream& ut, bool oppsett) {   //Skriver data til fil. Bool som er 
	ut << text << '\n';									   //standard false (ved skriv til STEDER.DTA).
	ut << 'V'<< '\n';									   //Settes til true ved skriving til ARR_XX.DTA
	ut << tilSalgs << ' ' << pris << ' ' << solgt << '\n'; //s� den ogs� skriver ut billetter.
	if(oppsett) {
		for ( int i = 1; i <= tilSalgs; i++) {
			ut << billett[i];
				if(i < tilSalgs) 
					ut << ' ';
		}
		ut << '\n';
	}
}

void Vrimle::skriv_billett_til_fil(int antbill, int n) {	//Skriver ut til BILLETT.DTA
	int total = pris*antbill;
	ofstream ut("BILLETTER.DTA", ios::app);

	ut << "-----------------------Billett------------------------\n";
	ut << "Kundenummer: " << n << '\n';
	ut << "St�plasser i sone: " << text << "\n";
	ut << "Pris per billett: "<< pris << "\n\n";
	ut << "Antall billetter kj�pt: " <<antbill << "\n\n";
	ut << "-------------------------------------------------------\n";
	ut << "Totalpris: " << total << "\n";
	ut << "-------------------------------------------------------\n\n\n";
}

void Vrimle::kjop_billett(int n) {
	int billetter, antbill;
	int ANTALL = tilSalgs - solgt;
	cout << "\nKundenummer: " << n;		//Viser kundenummer
	if (solgt < tilSalgs) {				//Sjekker om det er flere ledige plasser
		display_billetter();			//Skriver ut oppsett (med solgte og ledige plasser
										//til skjermen
		antbill = billetter = les("\Hvor mange billetter onsker kunden?", 1, ANTALL);
		solgt += billetter;						//Plusser p� solgt 
		for (int i = 1; i<= tilSalgs; i++) {	//G�r gjennom plassene og
			if(billett[i] == 0 && billetter != 0) {	//skriver inn kundenummeret
				billett[i] = n;						//der det er ledig plass
				billetter--;						//Teller ned billetter
			}
		}
		skriv_billett_til_fil(antbill, n);
		cout << "\n" << antbill << " billett(er) for kunde " << n << " lagt til!\n";
	}
	else
		cout << "\nDet er ikke flere ledige plasser i sonen!\n";
}