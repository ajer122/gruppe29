#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "vrimle.h"
#include "stoler.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"

using namespace std;
//Constructor for lesing fra fil
Stoler::Stoler(ifstream& inn, char* navn, bool oppsett) : Sone(inn, navn) {
	inn >> rader >> seter;
	if(oppsett) {			//Bool som er standard false (ved lesing fra STEDER.DTA), men settes til 
							//true ved bruk av les fra ARR_XX.DTA s� den ogs� leser inn billetter.
	billett = new int* [rader+1];  //Oppretter ny array for inter hvor alle
									//kundenummer (0 ved usolgt) leses inn fra fil.
	for ( int i = 1; i <= rader; i++)
		billett[i] = new int[seter+1];
	
	for ( int i = 1; i <= rader; i++)
		for ( int j = 1; j <= seter; j++)
			inn >> billett[i][j];
	}
}
//Constructor
Stoler::Stoler(char* navn) : Sone(navn) {
	rader = les("\nRader", 1, MAXRADER);
	seter = les("\nSeter", 1, MAXSETER);
	tilSalgs = (rader * seter);
	solgt = 0;
	pris = les("\nBillettpris: ", 1, MAXPRIS);	

	cout << "\nSone '" << navn << "' med " << rader << " rader og "
		 << seter << " seter til " << pris << ",- per billett opprettet";
}
//Constructor brukt ved kopiering av oppsett
Stoler::Stoler(Stoler & s) : Sone((Sone*) &s) {
	rader = s.rader;
	seter = s.seter;

	billett = new int* [rader+1]; 
	
	for ( int i = 1; i <= rader; i++)
		billett[i] = new int[seter+1];
	
	for ( int i = 1; i <= rader; i++)
		for ( int j = 1; j <= seter; j++)
			billett[i][j] = 0;
}
//Overloaded operator som sjekker sone-type
bool Stoler::operator == (char s) {
	if (s == 'S') return true;
	else return false;
}

Stoler::~Stoler() { 
}

void Stoler::display() { //Display-funksjon som viser grov oversikt 
	Sone::display();
	for ( int i = 1; i <= rader; i++) {
		cout << endl;
			for ( int j = 1; j <= seter; j++)
				cout << "== ";
	}
	cout << endl << endl;
	cout << seter*rader << " seter fordelt pa " << rader << " rader" << endl;
	cout << "\nAntall plasser: " << tilSalgs;
	cout << "\nPris per billett: " << pris;
}


void Stoler::display_billetter() {	//Display-funksjon som viser detaljert oversikt 
	Sone::display();				//med informasjon om salg av billetter i sonen
	int ledig = tilSalgs - solgt;
	for ( int i = 1; i <= rader; i++) { 
		cout << "\nRad " << i << ": " << endl;
		for ( int j = 1; j <= seter; j++) {
			if(billett[i][j] == 0) 
				cout << "[=====] ";
			else
				cout << "[" << billett[i][j] << "] ";
		}	
	}
	cout << endl << endl;
	cout << seter*rader << " seter fordelt pa " << rader << " rader" << endl;
	cout << "\nAntall billetter til salgs: " << ledig;
	cout << "\nPris per billett: " << pris;
}

void Stoler::skriv_til_fil(ofstream& ut, bool oppsett) { //Skriver data til fil. Bool som er 
	ut << text << '\n';									 //standard false (ved skriv til STEDER.DTA).
	ut << 'S'<< '\n';									 //Settes til true ved skriving til ARR_XX.DTA
	ut << tilSalgs << ' ' << pris << ' ' << solgt << '\n';//s� den ogs� skriver ut billetter.
	ut  << rader << ' ' <<  seter << '\n'; 
	if(oppsett) {
		for ( int i = 1; i <= rader; i++) {
			if(i>1) ut << '\n';
			for ( int j = 1; j <= seter; j++) {
				if(j>1) ut << ' ';
				ut << billett[i][j];
			}
		}
		ut << '\n';
	}
}

void Stoler::skriv_billett_til_fil(int rad, int antbill, int n) {	//Skriver ut til BILLETT.DTA
	int total = pris*antbill;
	ofstream ut("BILLETTER.DTA", ios::app);

	ut << "-----------------------Billett------------------------\n";
	ut << "Kundenummer: " << n << '\n';
	ut << "Sitteplasser i sone: " << text << "\n";
	ut << "Pris per billett: "<< pris << "\n\n";
	ut << "Antall billetter kj�pt: " <<antbill << "\n";
	ut << "Rad: " << rad << "\n\n";
	ut << "-------------------------------------------------------\n";
	ut << "Totalpris: " << total << "\n";
	ut << "-------------------------------------------------------\n\n\n";
}

void Stoler::kjop_billett(int n) {
	int billetter, rad, antbill;
	int ANTALL = tilSalgs - solgt;

	cout << "\nKundenummer: " << n; //Viser kundenummer
	if (solgt < tilSalgs) {			//Sjekker om det er flere ledige plasser
		display_billetter();		//Skriver ut oppsett (med solgte og ledige plasser) 
									//til skjermen
		antbill = billetter = les("Hvor mange billetter onsker kunden?", 1, ANTALL);
		rad = les("Pa hvilken rad onsker kunden a sitte?", 1, rader);

		solgt += billetter;						//Plusser p� solgt 
			for(int i = 1; i <= seter; i++) {	//G�r gjennom setene i raden og
			if(billett[rad][i] == 0 && billetter != 0) {//skriver inn kundenummeret
				billett[rad][i] = n;					//der det er ledig plass
				billetter--;							//Teller ned billetter
			}
		}
		skriv_billett_til_fil(rad, antbill, n);
		cout << "\n" << antbill << " billett(er) for kunde " << n << " lagt til!\n";
	}
	else
		cout << "\nDet er ikke flere ledige plasser i sonen!\n"; 
}