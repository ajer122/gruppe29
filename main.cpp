#if !defined(main_cpp)
#define main_cpp

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"
#include "arrangementer.h"
#include "steder.h"
#include "kunder.h"

using namespace std;

Steder stedbase;
Arrangementer arrangementbase;
Kunder kundebase;
	
// extern Steder stedbase;
// extern Arrangementer arrangementbase;
// extern Kunder kundebase;

int main() {	
	 kundebase.les_fra_fil();
	 arrangementbase.les_fra_fil();
	 stedbase.les_fra_fil();

	char valg;

	skriv_meny();
	valg = les_kommando();
	while (valg != 'Q') {
		switch (valg) {
			case 'S': case 'O': stedbase.handling(valg); break;
			case 'A': arrangementbase.handling(); break;
			case 'K': kundebase.handling(); break;
			default:  break;
		}
		skriv_meny();
		valg = les_kommando();
	}
}

#endif