#if !defined(__vrimle_H) 
#define __vrimle_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"

using namespace std;

class Vrimle : public Sone {
	private:
		int antall;
		char* def;
		int* billett;
	public:
		Vrimle(ifstream& inn, char* navn, bool oppsett = false);
		Vrimle(char* navn);
		Vrimle(Vrimle & v);
		bool operator == (char s);
		void display();
		void display_billetter();
		void skriv_til_fil(ofstream& ut,  bool oppsett = false);
		void skriv_billett_til_fil(int antbill, int n);
		void kjop_billett(int n);
};

#endif