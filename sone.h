#if !defined(__sone_H) 
#define __sone_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"

using namespace std;

class Sone : public Text_element {
	protected:
		int tilSalgs;
		int solgt;
		int pris;
	public: 
		Sone(ifstream& inn, char* navn);
		Sone(char* navn);
		Sone(Sone* s);
		virtual bool operator == (char s);
		void display();
		void skriv_til_fil(ofstream& ut);
		void les_fra_fil();
		virtual void billett_kjop();
	
};

#endif