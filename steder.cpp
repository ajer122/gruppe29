#include <iostream>
#include <cstring>
#include <string>
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "vrimle.h"
#include "stoler.h"
#include "funksjoner.h"
#include "meny_funksjoner.h"
#include "listtool2.h"
#include <fstream>

using namespace std;

//Constructor
Steder::Steder()  {
	StedListe = new List(Sorted);  //Initierer sortert liste med Steder
}

Steder::~Steder() {
	//Foreløpig tom
}

void Steder::nytt_sted() {
	Sted* temp;					 //Midlertidig sted-objekt
	char navn[NVNLEN];			 //Char for å lese inn navn
	
	les("\nStedsnavn", navn, NVNLEN);	//Leser inn navn
	
	if (!StedListe->in_list(navn)) {	//Sjekker om navnet er i bruk
		temp = new Sted(navn);			//Oppretter nytt sted-objekt
		StedListe->add(temp);			//Legger inn i listen
		cout << "\nStedet er opprettet"; 
	}
		else
		cout <<"\nDet finnes allerede et sted med dette navnet";
	skriv_til_fil();					//Skriver nye data til fil
}

void Steder::display() {
	StedListe->display_list();		//Kaller på alle steders display-funksjon
	cout << "\n\n Trykk Enter for a ga tilbake til menyen.....";
	cin.ignore();				//Stopper menyen fra å skrives umiddelbart for å lettere
								//lese store mengder data som vises på skjermen
}

void Steder::les_fra_fil() {
	int antSteder;					//Int for antall steder
	char navn[STRLEN];			    //Char for å lese inn navn
	Sted* temp;						//Midlertidig sted-objekt

	ifstream inn("STEDER.DTA");		//Finner filen
	if (inn) {						//Sjekker om filen eksisterer
		inn >> antSteder;			//Leser inn antall steder øverst i filen
		for(int i = 1; i <= antSteder; i++) {	//Går gjennom alle steder
			inn.ignore();
			inn.getline(navn, STRLEN);			//Leser inn navn 
			temp = new Sted(navn, inn);			//Oppretter nytt sted-objekt
			StedListe->add(temp);				//Legger inn i listen
		}
	cout << "\nLEST DATA FRA STEDER.DTA";
	}
	else
		cout << "\nFinner ikke STEDER.DTA";
	inn.close();
}

void Steder::handling(char a) {		//Meny for steder og oppsett
	char valg;

	if ( a == 'S') {
		cin >> valg;
		cin.ignore();
		switch (valg)  {
			case 'D':  display();   break;
			case 'N':  nytt_sted(); break;
			case 'S':				break;
			default: 				break;
		}	
	}
	if (a == 'O') {
		cin >> valg;
		cin.ignore();
		switch (valg)  {
			case 'D':  display_oppsett(); break;
			case 'N':  nytt_oppsett();    break;
			case 'E':  endre_oppsett();   break;
			case 'S':					  break;
			default:	                  break;
		}
	}
}

void Steder::nytt_oppsett() { 
	 Sted* temp;				//Midlertidig sted-objekt
	 char navn[NVNLEN];			//Char for å lese inn navn

	 les("\nStedsnavn", navn, NVNLEN);	//Leser inn navn

	 if (StedListe->in_list(navn)) {	        //Sjekker om stedet finnes
      	 temp = (Sted*) StedListe->remove(navn); //Henter ut sted-objektet
         temp->nytt_oppsett();					 //Kaller på funksjon for å lage
         StedListe->add(temp);   				 //nytt oppsett og legger objekt 
	 }											 //tilbake i listen
	 else
		cout <<"\nFinner ikke stedet";
	 skriv_til_fil();							//Skriver nye data til fil
}

void Steder::endre_oppsett() { 
	 Sted* temp;
	 char navn[NVNLEN];

	 les("\nStedsnavn", navn, NVNLEN);

	 if (StedListe->in_list(navn)) {
      	 temp = (Sted*) StedListe->remove(navn);   
         temp->endre_oppsett();
         StedListe->add(temp);   
	 }
	 else
		cout <<"\nFinner ikke stedet";
	 skriv_til_fil();
}

void Steder::display_oppsett() {
	 Sted* temp;
	 char navn[NVNLEN];

	 les("\nStedsnavn", navn, NVNLEN);

	 if (StedListe->in_list(navn)) {
      	 temp = (Sted*) StedListe->remove(navn);   
         temp->display_oppsett();
         StedListe->add(temp);   
	 }
	 else
		cout <<"\nFinner ikke stedet";	
}

bool Steder::sjekk_navn(char* navn) { //Sjekker om navn på et sted eksisterer for 
	char temp[STRLEN];				  //fved opprettelse av nytt arrangement
	strcpy(temp, navn);

	if (StedListe->in_list(temp)){
		return true;					//Returner true hvis stedet finnes
	}
	else
		return false;					//Returner false hvis stedet ikke finnes
}


//Henter ut et oppsett gjennom copy-constructoren Sted::kopier() og kopierer det inn i en  
List* Steder::hent_oppsett(char* navn, int nr) { //midlertidig liste som returneres.
	 Sted* temp;								 //Midlertidig sted-objekt
	 List* templist;						     //Midlertidig liste
	 templist = new List(Sorted);				 //Initierer listen		  
	 											  
	 if (StedListe->in_list(navn)) {			//Sjekker om stedet eksisterer
      	 temp = (Sted*) StedListe->remove(navn);//Henter ut sted-objektet
         templist = temp->kopier(nr);			//og sender det til copy-constructoren
         StedListe->add(temp);   				//Legger tilbake i listen
    }
    return templist;							//Returnerer den nye listen
}											    //med oppsett

void Steder::skriv_til_fil() {
	int antSteder = StedListe->no_of_elements(); //Int med antall steder
	Sted* temp;								     //Midlertidig sted-objekt

	ofstream ut("STEDER.DTA");					//Oppretter filnavn
		ut << antSteder << '\n'; 				//Skriver antall steder øverst i filen

		if (!StedListe->is_empty()) {			//Sjekker at det er objekter å skrive
			for (int i = 1; i <= antSteder; i++) {//Går gjennom alle steder
				temp = (Sted*)StedListe->remove_no(i);//Henter ut sted-objekt
				temp->skriv_til_fil(ut);		      //og sender det til sted sin skriv-funksjon
				StedListe->add(temp);				  //Legger objektet tilbake i listen
			}
		} 
}