#if !defined(meny_funksjoner_cpp)
#define meny_funksjoner_cpp

#include <iostream>									
#include <cstring>
#include <string>								
#include "funksjoner.h"	
#include "meny_funksjoner.h"								
#include "kunder.h"						
#include "steder.h"										
#include "arrangementer.h"						

using namespace std;

char les_kommando() {
  char ch;
  cin >> ch;
  cin.ignore();
  return(toupper(ch));
}

void skriv_meny(){ //setw er for kjedelig, det er tabulatorhopp som gjelder!
	char linje[NVNLEN] = "\n----------------------------------------------------------";
	
	cout << "\n\n" << linje;
	cout << "\n BILLETTKJOP (v 1.0)"		<< "\t\t\tQ for a avslutte";
	cout << "\n Folgende funksjoner er tilgjengelige:";
	cout << linje;

	cout << "\n KUNDE:"		<< "\t\t\t\tARRANGEMENT:";
	
	cout << "\n K D - Kunde Display"		<< "\t\tA D - Arrangement Display";
	cout << "\n K N - Kunde Ny"			<< "\t\t\tA N - Arrangement Nytt";
	cout << "\n K E - Kunde Endre"		<< "\t\tA K - Arrangement Kjop";
	cout << "\n K S - Kunde Slett"		<< "\t\tA R - Arrangement Returner";
	cout								<< "\n\t\t\t\tA S - Arrangement Slett";
	
	cout << linje;

	cout << "\n STED:"		<< "\t\t\t\tOPPSETT:";

	cout << "\n S D - Sted Display"		<< "\t\tO D - Oppsett Display";
	cout << "\n S N - Sted Nytt"			<< "\t\tO N - Oppsett Nytt";
	cout << "\n S E - Sted Endre"		<< "\t\tO E - Oppsett Endre";
	cout << "\n S S - Sted Slett	"		<< "\tO S - Oppsett Slett";

	cout << linje;
	cout << "\n\n Valg: ";
}

void meny_oppsett() {
	cout << "\n\nFolgende funksjoner er tilgjengelige:";
	cout << "\n\tN - Ny sone";
	cout << "\n\tE - Endre sone";
	cout << "\n\tK - Kopier oppsett";
	cout << "\n\tS - Slett oppsett (inaktiv)";
	cout << "\n\tD - Display oppsett";
	cout << "\n\tQ - Quit / avslutt;";
	cout << "\n\nValg: ";
}

void meny_sone() {
	cout << "\n\nSonetype:";
	cout << "\n\tS - Stoler";
	cout << "\n\tV - Vrimle";
	cout << "\n\nValg: ";
}

void meny_kunde_display() {
	cout << "\n\nFolgende funksjoner er tilgjengelige:";
	cout << "\n\tA - Vis alle kunder";
	cout << "\n\tK - Vis en kunde med kundenummer";
	cout << "\n\tN - Vis alle kunder med et spesifikt navn";
	cout << "\n\tQ - Quit / avslutt;";
	cout << "\n\nValg ";
}

void meny_arrangement_display(){
	cout << "\nFolgende funksjoner er tilgjengelige: ";
	cout << "\n\tA - Display alle arrangementer";
	cout << "\n\tB - Skriv inn hele/delere av arrangement";
	cout << "\n\tC - Skriv inn hele/delere av spillested";
	cout << "\n\tD - Skriv inn hele/deler av artist";
	cout << "\n\tE - Skriv inn dato på arrangementet";
	cout << "\n\tF - Skriv inn type arrangement";
	cout << "\n\tG - Vis alle data om et arrangementnummer";
	cout << "\n\nValg: ";

}

#endif
