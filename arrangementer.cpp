#include <iostream>
#include <cstring>
#include <fstream>
#include <string>
#include "steder.h"
#include "sted.h"
#include "sone.h"
#include "stoler.h"
#include "vrimle.h"
#include "funksjoner.h"
#include "arrangementer.h"
#include "arrangement.h"
#include "meny_funksjoner.h"
#include "listtool2.h"

using namespace std;

Arrangementer::Arrangementer() {
	ArrListe = new List(Sorted);	//Initierer sortert liste med arrangementer
	sisteArrNr = 100;				//Definerer arrangementsnummer
}

Arrangementer::~Arrangementer() {
}

void Arrangementer::nytt() {
	Arrangement* temp;			//Midlertidig arrangement-objekt
	extern Steder stedbase;		//Deklarer stedbase
	char sted[STRLEN];				
	char navn[STRLEN];

	les("\nVelg spillested", sted, STRLEN);

	if(stedbase.sjekk_navn(sted)){		//Sjekker at stedet eksisterer
		les("\nNavn pa nytt arrangement", navn, STRLEN);
		temp = new Arrangement(navn, sted, ++sisteArrNr);//Oppretter nytt arrangement og sender
		ArrListe->add(temp);							 //med navn, stedsnavn og arrangementsnummer
		skriv_til_fil();								 //Skriver nye data til fil
	}
	else
		cout <<"\nStedet eksisterer ikke!\n";
}

void Arrangementer::deler_display(){				
	char navn[STRLEN];
	Arrangement* temp;				//Midlertidig arrangement-objekt
	int antArr;

	antArr = ArrListe->no_of_elements();	//Teller opp antall objekter i listen.
	les("Skriv inn deler av Arrangementsnavn/Sted/Artist", navn, STRLEN);
	for(int i = 1; i<=antArr; i++){					  //G�r gjennom alle objektene
		temp = (Arrangement*) ArrListe->remove_no(i); //Henter ut objekt fra listen
		if(temp->deler_navn(navn)){					  //Sender til funksjonen som sjekker om navnet
			temp->display();						  //er likt eller ulikt, og sender til display hvis
		}											  //det returneres true
		ArrListe->add(temp);		//Legger objektet tilbake i listen
	}
}

void Arrangementer::nr_display(){
	int nr;
	int antArr;
	Arrangement* temp;					//Midlertidig arrangement-objekt
	antArr = ArrListe->no_of_elements(); //Teller opp antall objekter i listen
	nr = les("Skriv inn Arrangementnr",ARRNUMMER,MAXARRNUMMER);
	for(int i = 1; i<=antArr; i++){		//G�r gjennom alle objektene
		temp = (Arrangement*) ArrListe->remove_no(i); //Henter ut objekt fra listen
	if(temp->nr_sjekk(nr)){				//Sender til funksjonen som sjekker om nummeret
		temp->display_alt();			//er likt eller ulikt, og sender til display hvis
	}									//det returneres true
	ArrListe->add(temp);				//Legger tilbake i listen.
	}
	cout << "\n\n Trykk Enter for a ga tilbake til menyen.....";
	cin.ignore();				//Stopper menyen fra � skrives umiddelbart for � lettere
								//lese store mengder data som vises p� skjermen
}

void Arrangementer::type_display(){
	int type;
	int antArr;
	Arrangement* temp;					//Midlertidig arrangement-objekt
	antArr = ArrListe->no_of_elements(); //Teller opp antall objekter i listen
	type = les("Skriv inn type arrangement(Musikk, Sport, Kino, teater, Familie, Festival)\n",0,6);
	for(int i = 1; i<=antArr; i++){		//G�r gjennom alle objektene
		temp = (Arrangement*) ArrListe->remove_no(i); //Henter ut objekt fra listen
	if(temp->type_sjekk(type)){			//Sender til funksjonen som sjekker om type
		temp->display();				//er lik eller ulik, og sender til display hvis
	}									//det returneres true
	ArrListe->add(temp);				//Legger tilbake i listen
	}
}

void Arrangementer::dato_display(){
	int dato;
	int antArr;
	Arrangement* temp;					 //Midlertidig arrangement-objekt
	antArr = ArrListe->no_of_elements(); //Teller opp antall objekter i listen
	dato = les("Skriv inn dato for Arrangementet",MINDATO,MAXDATO);
	for(int i = 1; i<=antArr; i++){		//G�r gjennom alle objektene
		temp = (Arrangement*) ArrListe->remove_no(i); //Henter ut objekt fra listen
	if(temp->dato_sjekk(dato)){			//Sender til funksjonen som sjekker om dato
		temp->display();				//er lik eller ulik, og sender til display hvis
	}									//det returneres true
	ArrListe->add(temp);				//Legger tilbake i listen
	}
}

void Arrangementer::display() { //Meny for ulike display-muligheter
char valg;

	meny_arrangement_display();
	valg = les_kommando();
	switch (valg){
		case 'A' : ArrListe->display_list(); break;
		case 'B' : deler_display();	break;
		case 'C' : deler_display();	break;
		case 'D' : deler_display();	break;
		case 'E' : dato_display();	break;
		case 'F' : type_display();	break;
		case 'G' : nr_display();	break;
		default : break;
	}
	skriv_meny();
	valg = les_kommando();
}

void Arrangementer::endre_arrangement(int id) {
	//Forl�pig tom
}

void Arrangementer::billett() {
	char navn[STRLEN];
	int dato, antall, antArr;
	Arrangement* temp;						//Midlertidig arrangement-objekt
	antArr = ArrListe->no_of_elements();	//Teller opp antall objekter i listen.

	les("Arrangementets navn", navn, STRLEN);
	dato = les("Dato for arrangementet", MINDATO, MAXDATO);

	if (ArrListe->in_list(navn)) {			//Sjekker at arrangementet ligger i listen
		for(int i = 1; i <= antArr; i++) {	//G�r gjennom alle arrangementer
				temp = (Arrangement*) ArrListe->remove_no(i); //Henter ut arrangement 
				if(temp->deler_navn(navn)) {				//og sjekker om navnet er likt
					if (temp->dato_sjekk(dato)) {			//og deretter om dato er lik.
						temp->billett();					//Sender til neste billett-funksjon		   
						ArrListe->add(temp);				//Legger tilbake i listen
					}
					else {
						cout << "\nIngen arrangement p� denne datoen!\n";
						ArrListe->add(temp);			//Legger tilbake i listen
					}
				}
				else
					ArrListe->add(temp);				//Legger tilbake i listen
		}
	}
	else
		cout << "\nFinner ikke arrangement!\n";
}

void Arrangementer::returner() {
	//Forel�pig tom
}

void Arrangementer::skriv_til_fil() {
	Arrangement* temp;							//Midlertidig arrangement-objekt
	int antArr = ArrListe->no_of_elements();	//Teller opp antall objekter i listen.

	ofstream ut("ARRANGEMENT.DTA");				//Lager filnavn
		ut << sisteArrNr << '\n';				//Skriver ut siste brukte arrangementsnummer
		if (!ArrListe->is_empty()){				//Hvis listen ikke er tom g�r den
			for (int i = 1; i <= antArr; i++){	//gjennom alle arrangementer og sender videre
				temp = (Arrangement*)ArrListe->remove_no(i); //til aktuell skriv_til_fil-funksjon
				temp->skriv_til_fil(ut);	
				ArrListe->add(temp);			//Legger tilbake i listen
			}
		} 
		else
			cout << "Fil ikke funnet"; 
}

void Arrangementer::les_fra_fil() {
 	Arrangement* temp;			//Lager en Klassepeker temp;
 	char arrNavn[NVNLEN];

	ifstream inn("ARRANGEMENT.DTA"); //Finner fil
	if (inn) {						//Sjekker om filen finnes
		inn >> sisteArrNr;			//Leser inn siste arrangementnummer
		inn.ignore();				//Hopper over linjeskift
		for ( int i = ARRNUMMER+1; i <= sisteArrNr; i++) { //G�r gjennom alle arrangementer
			inn.getline(arrNavn, STRLEN);					//og senderer videre til
			temp = new Arrangement(inn, arrNavn);			//constructur i arrangement.
			ArrListe->add(temp);           //Legger innlest data Arrangementlisten.
		}
	cout << "\nLEST DATA FRA ARRANGEMENT.DTA";
	}
	else
		cout << "\nFinner ikke ARRANGEMENT.DTA";
}

void Arrangementer::handling(){	//Meny som leser bokstaven etter 'A
	char valg;

	cin >> valg;
	cin.ignore();

	switch (valg) {
		case 'D': display();		break;
		case 'N': nytt(); 			break;
		case 'E': skriv_til_fil();	break;
		case 'S': slett();			break;
		case 'K': billett();		break;
		case 'R':					break;
		default:					break;
	}
}

void Arrangementer::slett() {
	bool funnet = false;
	int nr, i;
	char fnavn[STRLEN];										// char array som inneholder fil navn
	char filbuffer[MAXARRNUMMER];									// buffer som er talllendge lang 
	Arrangement* temp;										// Arrangement peker
		
	cout << "hvilket arrangement vil du slette : ";			// sp�rr bruker om arr nr
	cin >> nr;												// tar imot arr nr
	for (i = 1; i <= ArrListe->no_of_elements(); i++){		// g�r gjennom alle elementene i listen
		temp = (Arrangement*)ArrListe->remove_no(i);		// tar nr i ut av listen og setter den i temp
		if (temp->nr_sjekk(nr)){						// sjekker om nr bruker ga finnes
			delete temp;									// sletter temp

			if(sisteArrNr == nr) --sisteArrNr;
			skriv_til_fil();

			strcpy(fnavn, "ARR_");							// skriver "Arr_" inn i fnavn
			strcat(fnavn, _itoa(nr, filbuffer, 10));		// limer p� nr, git av bruker p� fnavn
			strcat(fnavn, ".DTA");							// limer p� ".DTA" bakerst p� fnavn

			if (!remove(fnavn)){								// pr�ver � fjerne fil 
				cout << "arrangementet og datafil er slettet\n";	// skriver ut meld til bruker hvis han f�r slettet filen
			}
			else cout << "f�r ikke slettet filen";			// skriv ut meld til bruker
		}
		else	ArrListe->add(temp);						// legger temp tilbake i listen
	}
	if (!funnet)											// hvis funnet er false vet vi at han ikke kom inn i l�kka
		cout << "finner ikke arrangementet";				// skriver ut meld til bruker
}