#if !defined(__arrangement_H) 
#define __arrangement_H 

#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include "enum.h"
#include "const.h"
#include "funksjoner.h"
#include "listtool2.h"

//deklarasjon av 'Arrangement':

class Arrangement : public Text_element{
	private:
		char* spillested;
		char* artist;
		int dato;
  		int arrNr;
		int time;
		int min;
		List* oppsett;
  		arrtype typeArrangement;
	public:
		Arrangement(ifstream & inn, char* arrNavn);
		Arrangement(char* arrNavn, char* navn, int nummer);
		bool sted_display(char* n);
		bool dato_sjekk(int n);
		bool nr_sjekk(int n);
		bool type_sjekk(int n);
		void display_alt();
		void vis_enum();
		void billett();
		bool deler_navn(char* n);
		void display();
		void skriv_til_fil(ofstream& ut);
        void skriv_til_arr_fil();
		void les_fra_arr_fil();
		void enumtype();
};

#endif