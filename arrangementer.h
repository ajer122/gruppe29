#if !defined(__arrangementer_H) 
#define __arrangementer_H 

#include <iostream>
#include <cstring>
#include <string>
#include "const.h"
#include "funksjoner.h"
#include "listtool2.h"

//deklarasjon av 'Kunder':

class Arrangementer {
	private:
		int sisteArrNr; // Antall arrangementer hittil
		List* ArrListe;
	public:
		Arrangementer();
		~Arrangementer();
		void deler_display();
		void nr_display();
		void dato_display();
		void type_display();
		void display();
		void nytt ();
		void endre_arrangement (int id);
		void slett();
		void billett();
		void returner();
		void skriv_til_fil();
		void les_fra_fil();
		void handling();
};

#endif