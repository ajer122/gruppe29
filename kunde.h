#if !defined(__kunde_H)            //  Ensures that this file will be
#define __kunde_H                  //    included only ONCE in each file.

#include <iostream>
#include "listtool2.h"
#include "const.h"

using namespace std;

// Deklarasjon av klasse kunde:

class Kunde : public Num_element {
	private:
		int postnr, tlf;
		char *navn, *gateadr, *poststed, *mail;
	public:
		Kunde(ifstream & inn, int n); 
		Kunde(int ny_kunde);
		bool delerNavn(char* n);
		void display();
		void skriv_til_fil(ofstream & ut);
		void skriv_billett_til_fil(ofstream& ut, int n);
};

#endif