#if !defined(__const_H) 
#define __const_H 

#include <iostream>								
#include <cstring>
#include <string>

using namespace std;

//Globale 
const int NVNLEN = 80;
const int STRLEN = 40;
const int MAXOBJEKTER = 10;

//Steder
const int MAXPRIS = 1000;
const int MAXRADER = 10;
const int MAXSETER = 10;
const int MAXVRIMLE = 100;
const int MAXOPPSETT = 5;
const int MINOPPSETT = 1;

//Kunder
const int KUNDENR = 10000;

//Arrangement
const int MINDATO = 20000101;
const int MAXDATO = 22003112;
const int ARRNUMMER = 100;
const int MAXARRNUMMER = 999;

#endif
//Steder
//Kunder
//Arrangement