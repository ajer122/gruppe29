#include <iostream>				//Cin, cout
#include <fstream>
#include <cstring>				//Strcpy, strlen
#include <iomanip>				//setw, setfill
#include <string.h>
#include "const.h"
#include "listtool2.h"
#include "kunde.h"				//Klassedeklarasjon
#include "funksjoner.h"			//Globale funksjoner

using namespace std;

//Constructor for lesing fra fil
Kunde::Kunde(ifstream & inn, int n) : Num_element(n) {	
	char buffer[STRLEN];			//Bufferarray
	inn >> postnr >> tlf; 			//Leser inn postnummer og telefonnummer
	inn.ignore();
	inn.getline(buffer, STRLEN);	//Leser inn navn på kunde
	navn = new char[strlen(buffer)+1];
	strcpy(navn, buffer);
	inn.getline(buffer, STRLEN);	//Leser inn kundens adresse
	gateadr = new char[strlen(buffer)+1];
	strcpy(gateadr, buffer);
	inn.getline(buffer, STRLEN);	//Leser inn poststed
	poststed = new char[strlen(buffer)+1];
	strcpy(poststed, buffer);
	inn.getline(buffer, STRLEN);	//Leser inn kundens e-post
	mail = new char[strlen(buffer)+1];
	strcpy(mail, buffer);
}

//Constructor for ny kunde
Kunde::Kunde(int ny_kunde): Num_element(ny_kunde){
	char buffer[STRLEN];			//Bufferarray
	les("Skriv inn navn pa kunde", buffer, STRLEN);//Leser inn navn på kunde
	navn = new char [strlen(buffer)+1];	
	strcpy(navn, buffer);
	les("Skriv inn adresse", buffer, STRLEN);      //Leser inn kundens adresse
	gateadr = new char [strlen(buffer)+1];
	strcpy(gateadr, buffer);
	les ("Skriv inn poststed", buffer, STRLEN);	   //Leser inn poststed
	poststed = new char [strlen(buffer)+1];
	strcpy(poststed, buffer);
	les ("Skriv inn epost adresse", buffer, STRLEN);//Leser inn kundens e-post
	mail = new char [strlen(buffer)+1];
	strcpy(mail, buffer);
	postnr = les("Skriv inn postnummer", 100, 9999);		   //Leser inn postnummer
	tlf = les("Skriv inn telefonnummer", 10000000, 99999999); //Leser inn telefonnummer
}

void Kunde::display() {  //Skriver alle data om kunden til skjermen
  cout << "\n\nKundenummer: " << number;
  cout << "\nNavn: " << navn;
  cout << "\nAdresse: " << gateadr;
  cout << "\nPoststed: " << poststed;
  cout << "\nPostnummer: " << postnr;
  cout << "\nePost: " << mail;
  cout << "\nTelefonnummer: " << tlf;
}	

void Kunde::skriv_til_fil(ofstream& ut)	{ //Skriver ut alle data om kunden til fil
	ut << number << '\n' 				  //med kundenummer øverst
	   << postnr << ' '
	   << tlf << '\n'
	   << navn << '\n' 
	   << gateadr << '\n' 
	   << poststed << '\n' 
	   << mail << '\n';
}

void Kunde::skriv_billett_til_fil(ofstream& ut, int n){
	ut << "\n\n-----------------------Billetkjop----------------------\n\n";
	ut << "Tusen takk for bestillingen " << navn << "!!\n\n";
	ut << "Vi har folgende opplysninger om deg: \n\n";
	ut << "Kundenummer: " << n << '\n';
	ut << "Navn: " << navn << '\n';
	ut << "Adresse: "<< gateadr << ' ' << postnr << ' '<< poststed << '\n';
	ut << "Telefon nr og mail: " << tlf << ' ' << mail << "\n\n";

}

bool Kunde::delerNavn(char* n){ //Sender med char peker fra Kunder
	if(strstr(navn, n)){		//sjekker om deler av navn er like som sendt variabel
	return true;				//Om true, returnerer true.
	}
	else
		return false;			//Om false, returnerer false.

}